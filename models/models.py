# -*- coding: utf-8 -*-

from odoo import models, fields, api

class ProductDoctor(models.Model):
	_inherit = 'product.template'

	
	doctor_name = fields.Many2one('doctor.doctor', ondelete="cascade", string="Doctor Name")
