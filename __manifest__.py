# -*- coding: utf-8 -*-
{
    'name': "Doctor Service",

    'summary': """
        Menambahkan page baru pada product""",

    'description': """
        Meng-inherit product dan menambahkan page "Doctor" yang berisi field "Doctor Name" yang dapat
        diisi ketika product yang akan ditambahkan berupa "Service"
    """,

    'author': "eHealth",
    'website': "http://www.ehealth.id",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'E-Health',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','product','doctor'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/doctor_services.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}